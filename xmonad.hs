import XMonad
import qualified XMonad.StackSet as W
import System.IO (hPutStrLn)
import qualified Data.Map        as M

import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.RotSlaves
import XMonad.Actions.WithAll

import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ServerMode
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory

import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

import XMonad.Prompt
import XMonad.Prompt.Shell (shellPrompt)

import XMonad.Util.EZConfig
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.Scratchpad

myStartupHook :: X ()
myStartupHook = do
        spawnOnce "feh --randomize --bg-fill ~/wallpapers/*"  -- feh set random wallpaper"
        spawnOnce "setxkbmap -option caps:swapescape"
        spawnOnce "systemctl --user restart redshift.service"
        spawnOnce "brightnessctl s 1"
        setWMName "LG3D"

myTerminal      = "alacritty"
myEditor = "emacsclient -c -a emacs "  -- Sets emacs as editor for tree select
myBrowser = "firefox"
myMusic = " flatpak run com.spotify.Client"
myVidEdit = "flatpak run org.kde.kdenlive"
myScreenRec = "flatpak run com.obsproject.Studio"

myFont = "xft:Source Code Pro  Mono:regular:size=9:antialias=true:hinting=true"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

myBorderWidth   = 2

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myModMask       = mod4Mask

myWorkspaces    = ["origin","obs","empty"]

myNormColor   = "#282c34"   -- Border color of normal windows
myFocusColor  = "#46d9ff"   -- Border color of focused windows

shellXPConfig = greenXPConfig {

                               autoComplete      = Nothing--Just 100000    -- set Just 100000 for .1 sec
                               , height            = 80
                               ,promptKeymap = vimLikeXPKeymap
                              ,  position =  CenteredAt {xpCenterY = 0.19 , xpWidth = 0.88}
                               ,  font = "xft:Lucida MAC:size=22"
                       }
moveToList = [
           ("empty", moveTo Prev EmptyWS)
           ,("nonEmpty", moveTo Prev NonEmptyWS)
           ]

myKeys' = [
           -- ("M-<Return>",spawn myTerminal)
           ("M-<Return>", namedScratchpadAction myScratchPads "terminal")
           ,("M1-<Return>", namedScratchpadAction myScratchPads "terminal")
           ,("S-<Backspace>",kill)
           ,("M-<Backspace>",killAll)
           ,("M-a", makeGrid2 moveToList)
           ,("M1-]", moveTo Next NonEmptyWS)
           ,("M1-[", moveTo Prev NonEmptyWS)
           ,("M-]", nextWS)
           ,("M-[",  prevWS)
           ,("M-e",spawn myEditor)
           ,("M-q",makeGrid2 killList)
           ,("S-<Return>",makeGrid1 gridList)
           ,("S-<space>",makeGrid1 gridList)
           ,("M1-<Space>",makeGrid1 gridList)
           , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
           ,("M1-o",shellPrompt shellXPConfig)
           ,("M1-<Tab>", rotAllUp)--windows W.focusDown)
           ,("M-<Tab>", goToSelected def {gs_cellheight = 100,gs_cellwidth=400,gs_font="xft:Lucida MAC:size=24"})--windows W.focusDown)
           , ("M-S-n", sendMessage $ MT.Toggle NOBORDERS)      -- Toggles noborder
           , ("M-S-p", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
           , ("M-S-<Space>", sendMessage ToggleStruts)         -- Toggles struts
            ]

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "mocp" spawnMocp findMocp manageMocp
                , NS "spotify" spawnSpotify findSpotify manageSpotify
                ]
  where
    spawnTerm  = myTerminal -- ++ " -n scratchpad"
    findTerm   = className =? "Alacritty"--myTerminal
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.80
                 w = 0.84
                 t = 0.9 -h
                 l = 0.91 -w
    spawnMocp  = myTerminal ++ " -n mocp 'mocp'"
    findMocp   = resource =? "mocp"
    manageMocp = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnSpotify  =  "flatpak run com.Spotify.Client"-- ++ " -n scratchpad"
    findSpotify   = className =? "Spotify"--myTerminal
    manageSpotify = customFloating $ W.RationalRect l t w h
               where
                 h = 0.80
                 w = 0.84
                 t = 0.9 -h
                 l = 0.91 -w

redList = [
            ("restart",spawn "systemctl --user restart redshift.service")
            ,("stop",spawn "systemctl --user stop redshift.service")
            ,("open",spawn $ myEditor ++ " ~/.config/redshift/redshift.conf")
            ]

xmonadList :: [(String,X())]
xmonadList = [
                ("restart",spawn "xmonad --recompile ; xmonad --restart")
                ,("open",spawn $ myEditor ++ " ~/.xmonad/xmonad.hs")
             ]

exitList :: [(String,X())]
exitList = [
                ("shutdown",spawn "poweroff")
                ,("restart",spawn "reboot")
           ]

flatList  = [
                ("kdenlive",spawn $ myVidEdit)
                ,("obs",spawn $ myScreenRec)
               ]

gridList = [
            ("nothing",spawn "")
           ,("alacritty",namedScratchpadAction myScratchPads "terminal")
           --windows W.focusDown)
           ,("windows", goToSelected def {gs_cellheight = 100,gs_cellwidth=400,gs_font="xft:Lucida MAC:size=24"})
           ,("spotify", spawn $ myMusic)
           -- ,("spotifyy",namedScratchpadAction myScratchPads "spotify")
           ,("redshift",  makeGrid2 redList)
           ,("xmonad",  makeGrid2  xmonadList)
           ,("noBorders",sendMessage $ MT.Toggle NOBORDERS)
           ,("code",spawn $ myEditor)
           ,("firefox",spawn $ myBrowser)
           ,("exit",  makeGrid2  exitList)
           ,("flatpak",  makeGrid2  flatList)
           ,("kill",  makeGrid2  killList)
           ,("google",spawn "~/vc/firefox/google.sh")
           ,("youtube",spawn "~/vc/firefox/youtube.sh")
           ,("bluetooth",spawn "pavucontrol ; alacritty -e bluetoothctl")
           ,("nextLayout",sendMessage NextLayout)
           ,("xshell",shellPrompt shellXPConfig)
           ]

gsconfig1 colorizer =  (buildDefaultGSConfig colorizer){ gs_cellheight = 90,
                                                         gs_cellwidth = 150 ,
                                                         gs_navigate = mynavNSearch
                                                       ,gs_font = "xft:Lucida MAC:size=24"}

gsconfig2 colorizer =  (buildDefaultGSConfig colorizer){ gs_cellheight = 150,
                                                         gs_cellwidth = 230 ,
                                                         gs_navigate = mynavNSearch
                                                      ,gs_font = "xft:Lucida MAC:size=40"}
killList =    [
        ("one",kill)
      , ("all" , killAll)
      ,("workspace", removeWorkspace)
      ]

makeGrid1 = runSelectedAction (gsconfig1 myColorizer)
makeGrid2 = runSelectedAction (gsconfig2 myColorizer)

myColorizer :: a -> Bool -> X (String,String)
myColorizer _ True = return ("pink","#0f0f0f")
myColorizer _ False = return ("#0f0f0f","white")

myNavigation :: TwoD a (Maybe a)
myNavigation = makeXEventhandler $ shadowWithKeymap navKeyMap navDefaultHandler
 where navKeyMap = M.fromList [
          ((0,xK_Escape), cancel)
         ,((0,xK_Return), select)
         ,((0,xK_slash) , substringSearch navNSearch)--myNavigation)
         ,((0,xK_Left)  , move (-1,0)  >> myNavigation)
         ,((0,xK_h)     , move (-1,0)  >> myNavigation)
         ,((0,xK_Right) , move (1,0)   >> myNavigation)
         ,((0,xK_l)     , move (1,0)   >> myNavigation)
         ,((0,xK_Down)  , move (0,1)   >> myNavigation)
         ,((0,xK_j)     , move (0,1)   >> myNavigation)
         ,((0,xK_Up)    , move (0,-1)  >> myNavigation)
         ,((0,xK_y)     , move (-1,-1) >> myNavigation)
         ,((0,xK_i)     , move (1,-1)  >> myNavigation)
         ,((0,xK_n)     , move (-1,1)  >> myNavigation)
         ,((0,xK_m)     , move (1,-1)  >> myNavigation)
         ,((0,xK_space) , setPos (0,0) >> myNavigation)
         ]
       navDefaultHandler = const myNavigation

mynavNSearch = makeXEventhandler $ shadowWithKeymap navNSearchKeyMap navNSearchDefaultHandler
  where navNSearchKeyMap = M.fromList [
          ((0,xK_Escape) , cancel)
          ,((0,xK_Return)     , select)
          ,((0,xK_space)     , select)
          ,((0,xK_Left)       , move (-1,0) >> mynavNSearch)
          ,((0,xK_Right)      , move (1,0) >> mynavNSearch)
          ,((0,xK_Down)       , move (0,1) >> mynavNSearch)
          ,((0,xK_Up)         , move (0,-1) >> mynavNSearch)
          ,((0,xK_Tab)        , moveNext >> mynavNSearch)
          ,((shiftMask,xK_Tab), movePrev >> mynavNSearch)
          ,((0,xK_BackSpace), transformSearchString (\s -> if (s == "") then "" else init s) >> navNSearch)
          ]
        navNSearchDefaultHandler (_,s,_) = do
          transformSearchString (++ s)
          mynavNSearch

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
magnify  = renamed [Replace "magnify"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ magnifier
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           $ tabbed shrinkText myTabTheme
tallAccordion  = renamed [Replace "tallAccordion"]
           $ Accordion
wideAccordion  = renamed [Replace "wideAccordion"]
           $ Mirror Accordion

myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth tall
                                 ||| magnify
                                 ||| noBorders monocle
                                 ||| floats
                                 ||| noBorders tabs
                                 ||| grid
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow
                                 ||| tallAccordion
                                 ||| wideAccordion

myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , className =? "spotify"        --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore
    ] <+> namedScratchpadManageHook myScratchPads

myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
    where fadeAmount = 1.0

main = do
      xmproc <- spawnPipe "xmobar  $HOME/.config/xmobar/xmobarrc"
      xmonad $ ewmh def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormColor,
        focusedBorderColor = myFocusColor,

      -- key bindings
        -- keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = showWName' myShowWNameTheme myLayoutHook ,
        manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageDocks,
        handleEventHook    =  serverModeEventHookCmd
                               <+> serverModeEventHook
                               <+> serverModeEventHookF "XMONAD_PRINT" (io . putStrLn)
                               <+> docksEventHook,
        --myEventHook,
        -- logHook            = myLogHook,
        startupHook        = myStartupHook ,
        logHook = workspaceHistoryHook <+> myLogHook <+> dynamicLogWithPP xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc x
                        , ppCurrent = xmobarColor "#98be65" "" . wrap "[" "]" -- Current workspace in xmobar
                        , ppVisible = xmobarColor "#98be65" ""                -- Visible but not current workspace
                        , ppHidden = xmobarColor "#82AAFF" "" . wrap "*" ""   -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor "#c792ea" ""        -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor "#b3afc2" "" . shorten 60     -- Title of active window in xmobar
                        , ppSep =  "<fc=#666666> <fn=1>|</fn> </fc>"                     -- Separators in xmobar
                        , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
                        , ppExtras  = [windowCount]                           -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> ["<fc=#ffffff><fn=2>  \xf120 </fn></fc>"]++[ws,l]++ex++[t]
                        }
    }
           `additionalKeysP` myKeys'

myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Apple garamond:bold:size=90"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#000000"
    , swn_color             = "#FFFFFF"
    }
